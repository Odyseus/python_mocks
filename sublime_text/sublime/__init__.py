# -*- coding: utf-8 -*-
"""Mock module for Sublime Text's ``sublime`` module.

Attributes
----------
classes : dict
    Class names to register as mocks mapped to their docstrings.
"""
import sys

from sphinx.ext.autodoc.mock import _make_subclass


# NOTE: The commented lines are to make things less of a "wall of text". LOL
classes = {
    "Html": "Represents an HTML document.",
    #
    "ListInputItem": "Represents a row shown via :any:`ListInputHandler`.",
    #
    "Phantom": """Represents an HTML-based decoration to display non-editable content interspersed in a
``View``. Used with PhantomSet to actually add the phantoms to the ``View``.
Once a ``Phantom`` has been constructed and added to the ``View``, changes to the attributes
will have no effect.""",
    #
    "PhantomSet": """A collection that manages :any:`sublime.Phantom` and the process of adding them,
updating them and removing them from the ``View``.""",
    #
    "Region": "Represents an area of the buffer. Empty regions, where ``a == b`` are valid.",
    #
    "Settings": "A settings object.",
    #
    "View": """Represents a view into a text buffer. Note that multiple views may refer to the same buffer,
but they have their own unique selection and geometry.""",
    #
    "Window": "Represents an opened window.",
}


class SublimeMock:
    """``sublime`` module mock."""

    def __init__(self):
        """See :py:meth:`object.__init__`."""
        for cls_name, cls_doc in classes.items():
            setattr(self, cls_name, _make_subclass(cls_name, __name__))
            setattr(getattr(self, cls_name), "__doc__", cls_doc)

    def __getattr__(self, key):
        """See :py:meth:`object.__getattr__`.

        This method mocks all ``sublime`` module attributes.

        Parameters
        ----------
        key : str
            Attribute key.

        Returns
        -------
        str
            Hash.

        Raises
        ------
        AttributeError
            If the attribute key is not upper cased.
        """
        if key.isupper():
            return hash(key)
        else:
            raise AttributeError(key)


sys.modules[__name__] = SublimeMock()  # type: ignore[assignment]

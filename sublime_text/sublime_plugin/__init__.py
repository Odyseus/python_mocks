# -*- coding: utf-8 -*-
"""Mock module for Sublime Text's ``sublime_plugin`` module.

Attributes
----------
classes : dict
    Class names to register as mocks mapped to their docstrings.
"""
import sys

from sphinx.ext.autodoc.mock import _make_subclass


# NOTE: The commented lines are to make things less of a "wall of text". LOL
classes = {
    "ApplicationCommand": "Application commands are instantiated once per application start(?).",
    #
    "CommandInputHandler": """Note that many of these events are triggered by the buffer underlying the view,
and thus the method is only called once, with the first view as the parameter.""",
    #
    "EventListener": """A class that provides similar event handling to :any:`EventListener`, but bound to a specific view.
Provides class method-based filtering to control what views objects are created for.

The view is passed as a single parameter to the constructor. The default implementation
makes the view available via ``self.view``.""",
    #
    "ListInputHandler": """List input handlers objects can be used to accept a choice input from a list items in the
**Command Palette**. Return a subclass of this from the ``input()`` method of a command.

For an input handler to be shown to the user, the command returning the input handler
**MUST** be made available in the **Command Palette** by adding the command to a
**Default.sublime-commands** file.""",
    #
    "TextCommand": """Text commands are instantiated once per view. The :any:`sublime.View` object may be
retrieved via ``self.view``.""",
    #
    "TextInputHandler": """Text input handlers can be used to accept textual input in the **Command Palette**.
Return a subclass of this from the ``input()`` method of a command.

For an input handler to be shown to the user, the command returning the input handler
**MUST** be made available in the **Command Palette** by adding the command to a
**Default.sublime-commands** file.""",
    #
    "ViewEventListener": """A class that provides similar event handling to :any:`EventListener`, but bound to a specific view.
Provides class method-based filtering to control what views objects are created for.

The view is passed as a single parameter to the constructor. The default implementation
makes the view available via ``self.view``.""",
    #
    "WindowCommand": """Window commands are instantiated once per window. The :any:`sublime.Window` object may be retrieved
via ``self.window``.""",
}


class SublimePluginMock:
    """``sublime_plugin`` module mock."""

    def __init__(self):
        """See :py:meth:`object.__init__`."""
        for cls_name, cls_doc in classes.items():
            setattr(self, cls_name, _make_subclass(cls_name, __name__))
            setattr(getattr(self, cls_name), "__doc__", cls_doc)


sys.modules[__name__] = SublimePluginMock()  # type: ignore[assignment]
